module Util where

import Data.List (partition)
import System.FilePath (joinPath)
import Control.Monad (filterM)
import System.Directory (doesFileExist, getDirectoryContents)

tupleListEq :: Eq b => [(a, b)] -> [[(a, b)]] -> [[(a, b)]]
tupleListEq [] acc = acc
tupleListEq [x] acc = acc
tupleListEq ((x, y):xs) acc = tupleListEq neq (acc ++ eqAddSelf)
    where (eq, neq) = partition ((== y) . snd) xs
          eqAddSelf = [(x, y):eq | not (null eq)]

getDirectoryFiles :: FilePath -> IO [FilePath]
getDirectoryFiles d = do
  dContents <- getDirectoryContents d
  let fExists f = doesFileExist $ joinPath [d, f]
  filterM fExists [x | x <- dContents, x /= "..", x /= "."]
