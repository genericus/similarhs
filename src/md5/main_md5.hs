import Util
import MD5Similar

import Control.Monad
import System.Directory
import System.Environment (getArgs)

showMatches :: [[(String, b)]] -> IO ()
showMatches [[]] = return ()
showMatches [[x]] = return ()
showMatches ((first:rest):xs) = do
  putStr $ fst first
  mapM_ (\a -> putStr $ " == " ++ fst a) rest
  putStr "\n"
  showMatches xs
showMatches x = return ()

processDir' :: IO ()
processDir' = do
  current            <- getCurrentDirectory
  dirContentFiltered <- getDirectoryFiles current
  pairs              <- getFileMD5Pairs dirContentFiltered
  let equals   = tupleListEq pairs []
      len      = sum $ map length equals
      numDupes = len - length equals
  putStrLn $ "Found " ++ show numDupes ++ " duplicates in " ++ current
  showMatches equals

processDir :: FilePath -> FilePath -> IO ()
processDir origD d = do
  if d == "." || d == "./"      -- Special case of current dir
     then setCurrentDirectory origD
     else return ()
  exists <- doesDirectoryExist d
  if not exists
     then putStrLn $ "Directory does not exist: " ++ d
     else setCurrentDirectory d >> processDir'

main :: IO ()
main = do
  args <- getArgs
  curr <- getCurrentDirectory
  if (not . null) args
     then mapM_ (processDir curr) args
     else processDir curr curr
