module MD5Similar where

import qualified Data.ByteString.Lazy as BSL
import qualified Data.Digest.Pure.MD5 as MD5
import           Crypto.Classes (hash)
import           System.IO

import Control.Monad

getFileMD5 :: FilePath -> IO MD5.MD5Digest
getFileMD5 f = do
  h <- openFile f ReadMode
  contents <- BSL.hGetContents h
  BSL.length contents `seq` hClose h -- Force evaluation
  return $ hash contents

getFileMD5Pairs :: [FilePath] -> IO [(FilePath, MD5.MD5Digest)]
getFileMD5Pairs files = do
  md5s <- mapM getFileMD5 files
  return $ zip files md5s
