<!-- -*- mode: markdown; -*- -->
similarhs
---------

**similarhs** is a tool to find similar or identical files. Currently,
it only checks for files with the same MD5 sum (that is, files that
are 100% identical) but it is planned to add checks for like images
by taking samples.

The performance at the moment is not the best. It seems to consume a
lot of RAM, but it goes over a 400MB folder in 5-10 seconds, which I'm
happy with. I already find it very useful.

####What do I do with the results?

Right now, all you can do is check them yourself and delete the ones
you don't want. Later, I will add the ability to automatically delete
duplicates.
